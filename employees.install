<?php

module_load_include('inc', 'employees');

/**
 * Implements hook_install().
 */
function employees_install()
{
  employees_taxonomies_install();
  employees_node_type_install();
  employees_views_install();
}

/**
 * Implements hook_uninstall().
 */
function employees_uninstall()
{
  $bundle = EMPLOYEES_BUNDLE;
  @node_type_delete($bundle);
  @field_delete_instance('node', 'field_employees_name', $bundle);
  @field_delete_instance('node', 'field_employees_age', $bundle);
  @field_delete_instance('node', 'field_employees_salary_type', $bundle);
  @field_delete_instance('node', 'field_employees_remote_id', $bundle);

  $bundle = EMPLOYEES_TERM_BUNDLE;
  @field_delete_instance('taxonomy_term', 'field_salary_type_min', $bundle);
  @field_delete_instance('taxonomy_term', 'field_salary_type_max', $bundle);

  $vocabulary = taxonomy_vocabulary_machine_name_load($bundle);
  $terms = entity_load('taxonomy_term', FALSE, array('vid' => $vocabulary->vid));
  taxonomy_vocabulary_delete($vocabulary->vid);
  foreach ($terms as $term) taxonomy_term_delete($term->tid);
  variable_del('api_path_employees');

  if ($view = views_get_view(EMPLOYEES_VIEW_NAME)) views_delete_view($view);
}

/**
 * Create taxonomies
 */
function employees_taxonomies_install()
{
  $bundle = EMPLOYEES_TERM_BUNDLE;
  $vocabulary = new stdClass();
  $vocabulary->name = t('Salary type');
  $vocabulary->machine_name = $bundle;
  $vocabulary->description = t('Salary type taxonomía.');
  $vocabulary->module = 'taxonomy';
  taxonomy_vocabulary_save($vocabulary);
  $vid = $vocabulary->vid;


  $name = 'field_salary_type_min';
  if (!field_info_field($name)) {
    $field = array(
      'field_name' => $name,
      'type' => 'number_integer',
    );
    field_create_field($field);

    // Create the instance on the bundle.
    $instance = array(
      'field_name' => $name,
      'entity_type' => 'taxonomy_term',
      'label' => t('Salary min'),
      'bundle' => $bundle,
      'required' => TRUE,
      'widget' => array(
        'type' => 'textfield',
      ),
    );
    field_create_instance($instance);
  }

  $name = 'field_salary_type_max';
  if (!field_info_field($name)) {
    $field = array(
      'field_name' => $name,
      'type' => 'number_integer',
    );
    field_create_field($field);

    // Create the instance on the bundle.
    $instance = array(
      'field_name' => $name,
      'entity_type' => 'taxonomy_term',
      'label' => t('Salary max'),
      'bundle' => $bundle,
      'required' => TRUE,
      'widget' => array(
        'type' => 'textfield',
      ),
    );
    field_create_instance($instance);
  }

  //Create Terms
  $terms = array(
    array(
      'name' => 'Low Salary',
      'min' => 0,
      'max' => 1000
    ),
    array(
      'name' => 'Medium Salary',
      'min' => 1001,
      'max' => 4000
    ),
    array(
      'name' => 'High salary',
      'min' => 4000,
      'max' => 9999
    )
  );
  foreach ($terms as $obj) {
    if (!taxonomy_get_term_by_name($obj['name'], $vocabulary->machine_name)) {
      $term = new stdClass();
      $term->name = t($obj['name']);
      $term->vid = $vid;
      $term->field_salary_type_min[LANGUAGE_NONE][0]['value'] = $obj['min'];
      $term->field_salary_type_max[LANGUAGE_NONE][0]['value'] = $obj['max'];
      taxonomy_term_save($term);
    }
  }
}

/**
 * Create Node type
 */
function employees_node_type_install()
{
  $bundle = EMPLOYEES_BUNDLE;
  $info = new stdClass();
  $info->type = $bundle;
  $info->name = t('Employees');
  $info->base = 'node_content';
  $info->has_title = 1;
  $info->title_label = t('Title');
  $info->description = t('Employees node type');
  $info->custom = 1;
  $info->modified = 1;
  $info->locked = 0;
  $info->disabled = 0;
  $info->module = $bundle;
  node_type_save($info);

  $name = 'field_employees_remote_id';
  if (!field_info_field($name)) {
    $field = array(
      'field_name' => $name,
      'type' => 'number_integer',
    );
    field_create_field($field);

    // Create the instance on the bundle.
    $instance = array(
      'field_name' => $name,
      'entity_type' => 'node',
      'label' => t('Employe Remote ID'),
      'bundle' => $bundle,
      'required' => TRUE,
      'widget' => array(
        'type' => 'textfield',
      ),
    );
    field_create_instance($instance);
  }

  $name = 'field_employees_name';
  if (!field_info_field($name)) {
    $field = array(
      'field_name' => $name,
      'type' => 'text',
    );
    field_create_field($field);

    // Create the instance on the bundle.
    $instance = array(
      'field_name' => $name,
      'entity_type' => 'node',
      'label' => t('Employe name'),
      'bundle' => $bundle,
      'required' => TRUE,
      'widget' => array(
        'type' => 'textfield',
      ),
    );
    field_create_instance($instance);
  }

  $name = 'field_employees_age';
  if (!field_info_field($name)) {
    $field = array(
      'field_name' => $name,
      'type' => 'number_integer',
    );
    field_create_field($field);

    // Create the instance on the bundle.
    $instance = array(
      'field_name' => $name,
      'entity_type' => 'node',
      'label' => t('Employe age'),
      'bundle' => $bundle,
      'required' => TRUE,
      'widget' => array(
        'type' => 'textfield',
      ),
    );
    field_create_instance($instance);
  }

  $name = 'field_employees_salary_type';
  if (!field_info_field($name)) {
    $field = array(
      'field_name' => $name,
      'type' => 'taxonomy_term_reference',
      'settings' => array(
        'allowed_values' => array(
          array(
            'vocabulary' => EMPLOYEES_TERM_BUNDLE,
            'parent' => 0,
          ),
        ),
      ),
    );
    field_create_field($field);

    // Create the instance on the bundle.
    $instance = array(
      'field_name' => $name,
      'entity_type' => 'node',
      'label' => t('Employe Salary type'),
      'bundle' => $bundle,
      'required' => TRUE,
      'type' => 'taxonomy_term_reference',
      'widget' => array(
        'type' => 'taxonomy_autocomplete',
      ),
    );
    field_create_instance($instance);
  }

}

/**
 * Create Views
 */
function employees_views_install()
{
  $view = new view();
  $view->name = EMPLOYEES_VIEW_NAME;
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Filter terms';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: Content: Employe Salary type (field_employees_salary_type) */
  $handler->display->display_options['relationships']['field_employees_salary_type_tid']['id'] = 'field_employees_salary_type_tid';
  $handler->display->display_options['relationships']['field_employees_salary_type_tid']['table'] = 'field_data_field_employees_salary_type';
  $handler->display->display_options['relationships']['field_employees_salary_type_tid']['field'] = 'field_employees_salary_type_tid';
  $handler->display->display_options['relationships']['field_employees_salary_type_tid']['required'] = TRUE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['title']['link_to_node'] = FALSE;
  /* Sort criterion: Content: Employe name (field_employees_name) */
  $handler->display->display_options['sorts']['field_employees_name_value']['id'] = 'field_employees_name_value';
  $handler->display->display_options['sorts']['field_employees_name_value']['table'] = 'field_data_field_employees_name';
  $handler->display->display_options['sorts']['field_employees_name_value']['field'] = 'field_employees_name_value';
  $handler->display->display_options['sorts']['field_employees_name_value']['exposed'] = TRUE;
  $handler->display->display_options['sorts']['field_employees_name_value']['expose']['label'] = 'Employe name (field_employees_name)';
  /* Filter criterion: Content: Published status */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Employe name (field_employees_name) */
  $handler->display->display_options['filters']['field_employees_name_value']['id'] = 'field_employees_name_value';
  $handler->display->display_options['filters']['field_employees_name_value']['table'] = 'field_data_field_employees_name';
  $handler->display->display_options['filters']['field_employees_name_value']['field'] = 'field_employees_name_value';
  $handler->display->display_options['filters']['field_employees_name_value']['operator'] = 'starts';
  $handler->display->display_options['filters']['field_employees_name_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_employees_name_value']['expose']['operator_id'] = 'field_employees_name_value_op';
  $handler->display->display_options['filters']['field_employees_name_value']['expose']['label'] = 'Employe name (field_employees_name)';
  $handler->display->display_options['filters']['field_employees_name_value']['expose']['operator'] = 'field_employees_name_value_op';
  $handler->display->display_options['filters']['field_employees_name_value']['expose']['identifier'] = 'field_employees_name_value';
  $handler->display->display_options['filters']['field_employees_name_value']['expose']['remember_roles'] = array(
    2 => '2',
  );
  $handler->display->display_options['filters']['field_employees_name_value']['group_info']['label'] = 'Employe name (field_employees_name)';
  $handler->display->display_options['filters']['field_employees_name_value']['group_info']['identifier'] = 'field_employees_name_value';
  $handler->display->display_options['filters']['field_employees_name_value']['group_info']['remember'] = FALSE;
  $handler->display->display_options['filters']['field_employees_name_value']['group_info']['group_items'] = array(
    1 => array(),
    2 => array(),
    3 => array(),
  );
  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->display->display_options['path'] = 'taxonomy/term/%';
  $view->save();
}
